﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CSharpBasics.Utilities
{
	public class Calculator
	{
		/// <summary>
		/// Вычисляет сумму всех натуральных чисел меньше <see cref="number"/>, кратных любому из <see cref="divisors"/>
		/// </summary>
		/// <param name="number">Натуральное число</param>
		/// <param name="divisors">Числа, которым должны быть кратны слагаемые искомой суммы</param>
		/// <returns>Вычисленная сумма</returns>
		/// <exception cref="ArgumentOutOfRangeException">
		/// Выбрасывается в случае, если <see cref="number"/> или любое из чисел в <see cref="divisors"/> не является натуральным,
		/// а также если массив <see cref="divisors"/> пустой
		/// </exception>
		public float CalcSumOfDivisors(int number, params int[] divisors)
		{
			if (!IsNaturalNamber(number) || divisors.Length == 0)
				throw new ArgumentOutOfRangeException($"{nameof(number)} - не натуральное число");

			for (int i = 0; i < divisors.Length; i++)
            {
				if (!IsNaturalNamber(divisors[i]))
                {
					throw new ArgumentOutOfRangeException($"В массиве {nameof(divisors)} содержатся не натуральные числа");
                }
            }

			var sum = 0;
			List<int> multipe = new List<int>();
			for (int i = 0; i < divisors.Length; i++)
            {
				for (int j = number - 1; j > 0; j--)
				{
					if (j % divisors[i] == 0 && !multipe.Contains(j))
                    {
						multipe.Add(j);
						sum += j;
                    }
				}
            }

			return sum;
		}

		private bool IsNaturalNamber(int number)
        {
			return number > 0;
        }

		/// <summary>
		/// Возвращает ближайшее наибольшее целое, состоящее из цифр исходного числа <see cref="number"/>
		/// </summary>
		/// <param name="number">Исходное число</param>
		/// <returns>Ближайшее наибольшее целое</returns>
		public long FindNextBiggerNumber(int number)
		{
			if (number < 1)
				throw new ArgumentOutOfRangeException($"Исходное число {nameof(number)} меньше 1");

			var strNumber = number.ToString();
			var arrayNumbers = strNumber.Select(s => s).ToArray();
			var listNumbers = arrayNumbers.ToList();

			int indexReplace = -1;
			for (int i = listNumbers.Count - 1; i >= 1; i--)
			{
				if (listNumbers[i] > listNumbers[i - 1])
				{
					var temp = listNumbers[i];
					listNumbers[i] = listNumbers[i - 1];
					listNumbers[i - 1] = temp;
					indexReplace = i;
					break;
				}
			}

			if (indexReplace == -1)
				throw new InvalidOperationException("Ближайшего наибольшего числа не существует");

			var rangeLeftNumber = listNumbers.GetRange(0, indexReplace);
			var rangeRightNumber = listNumbers.GetRange(indexReplace, listNumbers.Count - indexReplace);
			rangeRightNumber.Sort();

			var resultList = new List<char>();
			resultList.AddRange(rangeLeftNumber);
			resultList.AddRange(rangeRightNumber);

			var newNumberString = string.Join("", resultList);
			var newNumber = int.Parse(newNumberString);

			if (newNumber == number)
				throw new InvalidOperationException("Ближайшего наибольшего числа не существует");

			return newNumber;
		}
	}
}