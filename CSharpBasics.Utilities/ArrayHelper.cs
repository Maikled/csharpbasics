﻿using System;
using System.Collections.Generic;

namespace CSharpBasics.Utilities
{
	public class ArrayHelper
	{
		/// <summary>
		/// Вычисляет сумму неотрицательных элементов в одномерном массиве
		/// </summary>
		/// <param name="numbers">Одномерный массив чисел</param>
		/// <returns>Сумма неотрицательных элементов массива</returns>
		/// <exception cref="ArgumentNullException"> Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public float CalcSumOfPositiveElements(int[] numbers)
		{
			if (numbers == null)
				throw new ArgumentNullException($"{nameof(numbers)} - null");

			var sum = 0;
			for (int i = 0; i < numbers.Length; i++)
            {
				if (numbers[i] >= 0)
					sum += numbers[i];
            }
			return sum;
		}

		/// <summary>
		/// Заменяет все отрицательные элементы в трёхмерном массиве на нули
		/// </summary>
		/// <param name="numbers">Массив целых чисел</param>
		/// <exception cref="ArgumentNullException"> Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public void ReplaceNegativeElementsBy0(int[,,] numbers)
		{
			if (numbers == null)
				throw new ArgumentNullException($"{nameof(numbers)} - null");

			for (int i = 0; i < numbers.GetLength(0); i++)
				for (int j = 0; j < numbers.GetLength(1); j++)
					for (int k = 0; k < numbers.GetLength(2); k++)
                    {
						if (numbers[i,j,k] < 0)
							numbers[i,j,k] = 0;
                    }
		}

		/// <summary>
		/// Вычисляет сумму элементов двумерного массива <see cref="numbers"/>,
		/// которые находятся на чётных позициях ([1,1], [2,4] и т.д.)
		/// </summary>
		/// <param name="numbers">Двумерный массив целых чисел</param>
		/// <returns>Сумма элементов на четных позициях</returns>
		/// <exception cref="ArgumentNullException"> Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public float CalcSumOfElementsOnEvenPositions(int[,] numbers)
		{
			if (numbers == null)
				throw new ArgumentNullException($"{nameof(numbers)} - null");

			var sum = 0;
			for (int i = 0; i < numbers.GetLength(0); i++)
				for (int j = 0; j < numbers.GetLength(1); j++)
                {
					if ((i + j) % 2 == 0)
                    {
						sum += numbers[i, j];
                    }
                }
			return sum;
		}

		/// <summary>
		/// Фильтрует массив <see cref="numbers"/> таким образом, чтобы на выходе остались только числа, содержащие цифру <see cref="filter"/>
		/// </summary>
		/// <param name="numbers">Массив целых чисел</param>
		/// <param name="filter">Цифра для фильтрации массива <see cref="numbers"/></param>
		/// <returns></returns>
		public int[] FilterArrayByDigit(int[] numbers, byte filter)
		{
			List<int> resultNumbersList = new List<int>();

			if (numbers != null)
            {
				for (int i = 0; i < numbers.Length; i++)
				{
					if (numbers[i].ToString().Contains(filter.ToString()))
					{
						resultNumbersList.Add(numbers[i]);
					}
				}
			}

			return resultNumbersList.ToArray();
		}
	}
}