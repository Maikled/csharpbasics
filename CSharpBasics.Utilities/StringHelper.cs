﻿using System;
using System.Collections.Generic;

namespace CSharpBasics.Utilities
{
	public class StringHelper
	{
		/// <summary>
		/// Вычисляет среднюю длину слова в переданной строке без учётов знаков препинания и специальных символов
		/// </summary>
		/// <param name="inputString">Исходная строка</param>
		/// <returns>Средняя длина слова</returns>
		public int GetAverageWordLength(string inputString)
		{
			if (inputString == "" || inputString == null)
				return 0;

			string str = "";
			int sumWords = 0, countWords = 0;
			for (int i = 0; i < inputString.Length; i++)
            {
				if (char.IsLetter(inputString[i]))
				{
					str += inputString[i];
				}
				if (char.IsWhiteSpace(inputString[i]) || i == inputString.Length - 1)
                {
					int strLenght = str.Length;
					str = "";
					if (strLenght > 0)
                    {
						sumWords += strLenght;
						countWords++;
                    }
                }
			}

			if (sumWords == 0 || countWords == 0)
				return 0;

			return sumWords / countWords;
		}

		/// <summary>
		/// Удваивает в строке <see cref="original"/> все буквы, принадлежащие строке <see cref="toDuplicate"/>
		/// </summary>
		/// <param name="original">Строка, символы в которой нужно удвоить</param>
		/// <param name="toDuplicate">Строка, символы из которой нужно удвоить</param>
		/// <returns>Строка <see cref="original"/> с удвоенными символами, которые есть в строке <see cref="toDuplicate"/></returns>
		public string DuplicateCharsInString(string original, string toDuplicate)
		{
			if (toDuplicate == null || toDuplicate == "" || original == null || original == "")
				return original;

			int i;
			List<char> symbols = new List<char>();
			for (i = 0; i < original.Length; i++)
            {
				char lowerSymbol = char.ToLower(original[i]);
				char upperSymbol = char.ToUpper(original[i]);
				if ((toDuplicate.Contains(lowerSymbol) || toDuplicate.Contains(upperSymbol)) && !symbols.Contains(original[i]))
                {
					string doubleSymbol = "" + original[i] + original[i];
					original = original.Replace(original[i].ToString(), doubleSymbol);
					symbols.Add(original[i]);
					i++;
				}
            }

			return original;
		}
	}
}